console.log("====1. Animal Class====");
console.log(

);
class Animal {
    constructor(legs, cold_blooded) {
        this.legs = 4
        this.cold_blooded = false
        this.name = "shaun"
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false
console.log(

);
class Ape extends Animal{
    constructor(nama, cold_blooded, legs){
    super (nama, cold_blooded)
    this.legs = 2
    }
get kaki(){
    return this.kaki
}
set kaki(n){
    return this.kaki=n
}
yell =() => {
    console.log("Auooo");
}
}
console.log(

);
class Frog extends Animal{
    constructor(nama, cold_blooded, legs){
    super (nama, cold_blooded)
    this.legs = 4
    }
get kaki(){
    return this.kaki
}
set kaki(n){
    return this.kaki=n
}
jump =() => {
    console.log("hop hop");
}
}


 
var sungokong = new Ape("kera sakti")
console.log("kera sakti");
console.log(sungokong.legs);
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
console.log("buduk");
console.log(kodok.legs);
kodok.jump() // "hop hop" 
console.log(

);
console.log("====2. Function to Class====");
console.log(

);
function Clock({ template }) {
  
    var timer;
  
    function render() {
      var date = new Date();
  
      var hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      var mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
  
      var secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
  
      var output = template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    }
  
    this.stop = function() {
      clearInterval(timer);
    };
  
    this.start = function() {
      render();
      timer = setInterval(render, 1000);
    };
  
  }
  
  var clock = new Clock({template: 'h:m:s'});
  clock.start(); 
  class Clock {

}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 